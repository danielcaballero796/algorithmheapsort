/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import ufps.util.colecciones_seed.Secuencia;
import model.Sort;

/**
 *
 * @author danis
 */
public class Main {
    
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
        Secuencia<Integer> x = new Secuencia<Integer>(10);
        x.insertar(65);
        x.insertar(4);
        x.insertar(15);
        x.insertar(9);
        x.insertar(10);
        x.insertar(34);
        x.insertar(62);
        x.insertar(66);
        x.insertar(68);
        x.insertar(70);
       
        
        
        Sort prueba = new Sort(x);
        prueba.heapSort();
        System.out.println(prueba.getVector().toString());
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin-inicio) / 1000);

        System.out.println(tiempo + " segundos");
    }
    
}
