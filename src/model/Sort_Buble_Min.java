package model;

import ufps.util.colecciones_seed.Secuencia;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author danis
 */
public class Sort_Buble_Min<T extends Comparable<T>> {

    private Secuencia<T> vector;
    String respuesta="";

    public Sort_Buble_Min(Secuencia<T> vtc) {
        this.vector = new Secuencia(vtc.getTamanio() + 1);
        Object x = 0;
        this.add((T) x);
        for (int i = 0; i < vtc.getTamanio(); i++) {
            this.add(vtc.get(i));
        }
    }

    public void add(T info) {
        this.vector.insertar(info);
        int n = this.vector.getTamanio();
        buble_Min(n);
    }

    public void buble_Min(int n) {

        int mid = n / 2;
        int aux = 0;

        for (int j = 0; j < (mid - 1); j++) {
            if (this.vector.get(j).compareTo(this.vector.get(n - j - 1)) >= 0) {
                swap(j, (n - j - 1));
            }
        }

        for (int i = 0; i < (mid - 1); i++) {

            for (int p = 0; p < mid; p++) {

                if (this.vector.get(p).compareTo(this.vector.get(p + 1)) >= 0) {
                    swap(p, (p + 1));
                    aux = 1;
                }

                if (this.vector.get(n - p - 1).compareTo(this.vector.get(n - p - 2)) <= 0) {
                    swap((n - p - 1), (n - p - 2));
                    aux = 1;
                }
            }
            if (aux == 0) {
                break;
            }
            aux = 0;
        }

    }

    public void swap(int dad, int son) {
        T temp = this.vector.get(dad);
        this.vector.set(dad, this.vector.get(son));
        this.vector.set(son, temp);
        respuesta+= "Operaciones para la insercion BubbleMin ";
        respuesta+= "dad = " + this.vector.get(dad) + " son = " + this.vector.get(son);
        respuesta+= "\n";
    }

    public Secuencia<T> getVector() {
        return vector;
    }
    
    public String respuestaBubbleMin(){
        return respuesta+"\n"+this.getVector().toString();
    }
}
