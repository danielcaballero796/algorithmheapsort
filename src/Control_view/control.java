/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control_view;

import model.Sort;
import com.itextpdf.text.DocumentException;
import model.Sort_Buble_Min;
import pdf.GenerarPdf;
import ufps.util.colecciones_seed.Secuencia;

/**
 *
 * @author Jaimes Rodriguez
 */
public class control<T> {

    Sort ss;
    Sort_Buble_Min bm;
    Secuencia info;
    GenerarPdf pdf;

    public control() {
    }

    public String adDatos(String dato) {
        long inicio = System.currentTimeMillis();
        String[] x = dato.split(",");
        info = new Secuencia(x.length + 1);
        Integer tmp = 0;
        info.insertar(tmp);
        for (int i = 0; i < x.length; i++) {
            info.insertar(Integer.parseInt(x[i]));
        }
        ss = new Sort(info);
        ss.heapSort();
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin-inicio) / 1000);        
        return ss.respuestaHeapSort()+" "+tiempo;
    }
    
    public String adDatosBuble(String dato) {
        long inicio = System.currentTimeMillis();
        String[] x = dato.split(",");
        info = new Secuencia(x.length + 1);
        for (int i = 0; i < x.length; i++) {
            info.insertar(Integer.parseInt(x[i]));
        }
        
        bm = new Sort_Buble_Min(info);
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin-inicio) / 1000);        
        return bm.respuestaBubbleMin()+" "+tiempo;
    }
    
    public String pdf(String dato) throws DocumentException {
        String[] x = dato.split(",");
        info = new Secuencia(x.length + 1);
        Integer tmp = 0;
        info.insertar(tmp);
        for (int i = 0; i < x.length; i++) {
            info.insertar(Integer.parseInt(x[i]));
        }
        pdf = new GenerarPdf(info);
        pdf.generar();
        return "¡Se ha generado el PDF!";
    }

    public String random() {
        int x = (int) (Math.random() * 50 + 1);
        info = new Secuencia(x + 1);
        Integer tmp = 0;
        info.insertar(tmp);
        for (int i = 0; i < x; i++) {
            info.insertar((int) (Math.random() * 34 + i));
        }
        ss = new Sort(info);
        bm = new Sort_Buble_Min(info);
        ss.heapSort();
        return ss.respuestaHeapSort()+"-"+bm.respuestaBubbleMin();
    }
    
}
